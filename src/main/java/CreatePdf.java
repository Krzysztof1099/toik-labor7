import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class CreatePdf {

    public static void main(String... args) throws FileNotFoundException, DocumentException {

        // 1. Create document
        Document document = new Document(PageSize.A4, 50, 50, 50, 50);

        // 2. Create PdfWriter
        PdfWriter.getInstance(document, new FileOutputStream("resume.pdf"));

        // 3. Open document
        document.open();

        // 4. Add title
        Font font = new Font(Font.FontFamily.TIMES_ROMAN,40.0f);
        Paragraph title = new Paragraph("RESUME",font);
        title.setAlignment(Element.ALIGN_CENTER);
        document.add(title);
        document.add( Chunk.NEWLINE );

        // 5. Add content
        PdfPTable table = new PdfPTable(2);
        table.addCell("First name");
        table.addCell("Krzysztof");
        table.addCell("Last name");
        table.addCell("Piechnik");
        table.addCell("Profession");
        table.addCell("Student");
        table.addCell("Education");
        table.addCell("2018 - present PWSZ Tarnów");
        table.addCell("Summary");
        table.addCell("I'm IT student. I'm familiar with a lot of languages and software used in programing");
        document.add(table);

        // 6. Close document
        document.close();

    }
}